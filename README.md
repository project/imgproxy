# CRON SERVICE

## INTRODUCTION
imgproxy is a fast and secure standalone server for resizing and converting
remote images. The main principles of imgproxy are simplicity, speed, and
security.

imgproxy can be used to provide a fast and secure way to replace all the image
resizing code of your web application (like calling ImageMagick or
GraphicsMagick, or using libraries), while also being able to resize everything
on the fly, fast and easy. imgproxy is also indispensable when handling lots of
image resizing, especially when images come from a remote source.

## DESCRIPTION
This module provides a twig extension which will take your image url
and resize settings and create a compiled and signed imgproxy url for you.
This module doesn't work with images by itself. It doesn't work with
Drupal ImageToolkit or Drupal image styles. It only creates an url to imgproxy
server according to given settings. Imgproxy server also should be installed by
your own.

#REQUIREMENTS
This module requires an imgproxy server to work with.

#INSTALLATION
`composer require drupal/cron_service` or other standard way of installing a
drupal module.
Deploy imgproxy server how it described
[here](https://github.com/imgproxy/imgproxy/blob/master/docs/installation.md).

#CONFIGURATION
Go to Configuration > Media > Imgproxy and set up an url to imgproxy server
and optionally set key and salt for signing the urls.
More about configuration imgproxy server you can find
[here](https://github.com/imgproxy/imgproxy/blob/master/docs/configuration.md).

## USAGE
In your template you can use `imgproxy` twig function like this:
```
imgproxy(item.content, {'height': 400, 'width': 400, 'resize': 'fill', 'gravity': 'sm'})
```
where `item.content` can be a field of FileItem type, \Drupal\file\Entity\File
instance, \Drupal\Core\Url instance, or an array with `#item` or `#url` key.

You can also use drupal `imgproxy` service or instantiate
`\Drupal\imgproxy\Imgproxy\ImgProxyUrl` class directly for creating urls.
