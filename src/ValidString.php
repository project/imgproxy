<?php

namespace Drupal\imgproxy;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class used to store string value with validation functionality.
 *
 * You must override the geAllowedValues() or ValidateValue() method to use it.
 */
abstract class ValidString {

  use StringTranslationTrait;

  /**
   * Position value.
   *
   * @var string
   */
  protected $value;

  /**
   * ValidString constructor.
   *
   * @param string $value
   *   New value.
   */
  public function __construct(string $value) {
    $this->setValue($value);
  }

  /**
   * Returns string representation of the object.
   *
   * @return string
   *   The value.
   */
  public function toString(): string {
    return $this->value;
  }

  /**
   * Checks whether the value is allowed.
   *
   * @param string $value
   *   Value to validate.
   *
   * @return bool
   *   TRUE if value is allowed.
   */
  protected function validateValue(string $value): bool {
    return in_array(
      $value,
      $this->getAllowedValues()
    );
  }

  /**
   * Returns the list of allowed values.
   *
   * @return array
   *   Allowed values.
   */
  protected function getAllowedValues(): array {
    return [];
  }

  /**
   * Setter for the position value.
   *
   * @param string $value
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setValue(string $value): self {
    if (!$this->validateValue($value)) {
      throw new \InvalidArgumentException(
        $this->t(
          'The value value @value passed to @class is not allowed',
          ['value' => $value, 'class' => static::class]
        )
      );
    }
    $this->value = $value;
    return $this;
  }

  /**
   * Getter for the position value.
   *
   * @return string
   *   Current value.
   */
  public function getValue(): string {
    return $this->value;
  }

  /**
   * Returns string representation of the object.
   *
   * @return string
   *   String representation of the object.
   */
  public function __toString() {
    return $this->toString();
  }

}
