<?php

namespace Drupal\imgproxy\Twig;

use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\imgproxy\ImgProxy;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension for imgproxy related functions.
 */
class TwigExtension extends AbstractExtension {

  /**
   * Imgproxy service.
   *
   * @var \Drupal\imgproxy\ImgProxy
   */
  protected $service;

  /**
   * TwigExtension constructor.
   *
   * @param \Drupal\imgproxy\ImgProxy $imgProxy
   *   Imgproxy service injection.
   */
  public function __construct(ImgProxy $imgProxy) {
    $this->service = $imgProxy;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('imgproxy', [$this, 'imgproxy']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'imgproxy';
  }

  /**
   * Creates imgproxy url for the given image.
   *
   * @param mixed $image
   *   Image as file entity, image item, or image formatter render array.
   * @param array $settings
   *   Transformation options.
   *
   * @return string
   *   Imageproxy url for the given image.
   *
   * @see \Drupal\imgproxy\ImgProxy::getUrl()
   */
  public function imgproxy($image, array $settings) {
    return $this->service->getUrl($this->suggestImageUrl($image), $settings);
  }

  /**
   * Extracts file url from image data.
   *
   * @param mixed $image
   *   Image as file entity, image field item or iamge formatter render array.
   *
   * @return string
   *   Url to the image.
   */
  protected function suggestImageUrl($image) {
    // Check render arrays first.
    if (is_array($image)) {
      // #item is used in image_formatter.
      if ($image['#item'] ?? NULL instanceof FileItem) {
        $image = $image['#item'];
      }
      // Take a file entity from it to process further.
      elseif ($image['#uri'] ?? NULL) {
        // #uri is used in image.
        return $image['#uri'];
      }
    }
    // If it's an image field. Or any other file field. We don't care.
    if ($image instanceof FileItem) {
      $image = $image->entity;
    }
    // It's a file entity.
    if ($image instanceof File) {
      $image = $image->getFileUri();
    }
    return $image;
  }

}
