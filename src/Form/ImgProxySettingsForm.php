<?php

namespace Drupal\imgproxy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for imgproxy.
 */
class ImgProxySettingsForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['imgproxy.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'imgproxy_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('imgproxy.settings');
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $config->get('endpoint'),
    ];
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $config->get('key'),
    ];
    $form['salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salt'),
      '#default_value' => $config->get('salt'),
    ];
    $form['local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use local system ImgProxy setup'),
      '#default_value' => $config->get('local'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('imgproxy.settings')
      ->set('endpoint', $form_state->getValue(rtrim('endpoint', '/')))
      ->set('key', $form_state->getValue('key'))
      ->set('salt', $form_state->getValue('salt'))
      ->set('local', $form_state->getValue('local'))
      ->save();
    $this->messenger()->addStatus($this->t('Settings were saved successfully'));
  }

}
