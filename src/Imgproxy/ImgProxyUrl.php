<?php

namespace Drupal\imgproxy\Imgproxy;

use InvalidArgumentException;

/**
 * Class for creating imgproxy urls.
 *
 * All the nullable fields here are not required by the imgproxy. If a field
 * is null, it will not be passed to the url string. However "empty" values like
 * boolean false, 0 or empty string WILL be passed to the target url string.
 */
class ImgProxyUrl {

  /**
   * Imgproxy endpoint.
   *
   * @var string
   */
  private $endpoint;

  /**
   * Imgproxy key.
   *
   * @var string
   */
  private $key;

  /**
   * Imgproxy salt.
   *
   * @var string
   */
  private $salt;

  /**
   * Target image width.
   *
   * @var int|null
   */
  private $width;

  /**
   * Target image height.
   *
   * @var int|null
   */
  private $height;

  /**
   * Should target image be enlarged.
   *
   * @var bool|null
   */
  private $enlarge;

  /**
   * Should the target image be extended.
   *
   * @var bool|null
   */
  private $extend;

  /**
   * Resize type.
   *
   * @var \Drupal\imgproxy\Imgproxy\ResizeType|null
   */
  private $resize;

  /**
   * Gravity direction on resizing.
   *
   * @var \Drupal\imgproxy\Imgproxy\Gravity|null
   */
  private $gravity;

  /**
   * Target extension.
   *
   * @var string|null
   */
  private $extension;

  /**
   * Target dpr.
   *
   * @var int|null
   */
  private $dpr;

  /**
   * Source image url.
   *
   * @var string
   */
  private $imageUrl;

  /**
   * Quality of the target image.
   *
   * @var int|null
   */
  private $quality;

  /**
   * Background color as hex.
   *
   * @var string
   */
  private $background;

  /**
   * Sigma value for blur.
   *
   * @var float|null
   */
  private $blur;

  /**
   * Sigma value for sharpen.
   *
   * @var float|null
   */
  private $sharpen;

  /**
   * Watermark information.
   *
   * @var \Drupal\imgproxy\Imgproxy\Watermark|null
   */
  private $watermark;

  /**
   * Cachebuster.
   *
   * @var string|null
   */
  private $cachebuster;

  /**
   * Presets.
   *
   * @var string[]|null
   */
  private $presets;

  /**
   * Setter for the endpoint field.
   *
   * @param string $endpoint
   *   A new endpoint.
   *
   * @return self
   *   Self.
   */
  public function setEndpoint(string $endpoint): self {
    $this->endpoint = $endpoint;
    return $this;
  }

  /**
   * Setter for the encryption key.
   *
   * @param string|null $key
   *   Hex-encoded string. Can be null for disabling encryption.
   *
   * @return self
   *   self.
   */
  public function setKey(?string $key): self {
    if ($key) {
      $key = pack('H*', $key);
      if ($key) {
        $this->key = $key;
      }
      else {
        throw new InvalidArgumentException('Key must be a hex-encoded string');
      }
    }
    else {
      $this->key = NULL;
    }
    return $this;
  }

  /**
   * Setter for the encryption salt.
   *
   * @param string|null $salt
   *   Hex-encoded string. Can be null for disabling encryption.
   *
   * @return self
   *   Self.
   */
  public function setSalt(?string $salt): self {
    // Check for the empty string.
    if ($salt) {
      $salt = pack('H*', $salt);
      // Check the packing was successful.
      if ($salt) {
        $this->salt = $salt;
      }
      else {
        throw new InvalidArgumentException('Salt must be a hex-encoded string');
      }
    }
    else {
      $this->salt = NULL;
    }
    return $this;
  }

  /**
   * Setter for the width field.
   *
   * Be aware that the width of the processed image will also depend on the
   * value of the resize and enlarge fields.
   *
   * @param int|null $width
   *   New width.
   *
   * @return self
   *   Self.
   */
  public function setWidth(?int $width): self {
    $this->width = $width;
    return $this;
  }

  /**
   * Setter for the height field.
   *
   * Be aware that the height of the processed image will also depend on the
   * value of the resize and enlarge fields.
   *
   * @param int|null $height
   *   New height.
   *
   * @return self
   *   Self.
   */
  public function setHeight(?int $height): self {
    $this->height = $height;
    return $this;
  }

  /**
   * Setter for the enlarge field.
   *
   * @param bool|null $enlarge
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setEnlarge(?bool $enlarge): self {
    $this->enlarge = $enlarge;
    return $this;
  }

  /**
   * Setter for the resize field.
   *
   * @param \Drupal\imgproxy\Imgproxy\ResizeType|null $type
   *   New resize type.
   *
   * @return self
   *   Self.
   */
  public function setResize(?ResizeType $type): self {
    $this->resize = $type;
    return $this;
  }

  /**
   * Setter for the gravity field.
   *
   * @param \Drupal\imgproxy\Imgproxy\Gravity|null $gravity
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setGravity(?Gravity $gravity): self {
    $this->gravity = $gravity;
    return $this;
  }

  /**
   * Setter for the extension field.
   *
   * @param string|null $extension
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setExtension(?string $extension): self {
    $this->extension = $extension;
    return $this;
  }

  /**
   * Setter for the extend field.
   *
   * @param bool|null $extend
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setExtend(?bool $extend): self {
    $this->extend = $extend;
    return $this;
  }

  /**
   * Setter for the dpr field.
   *
   * @param int|null $dpr
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setDpr(?int $dpr): self {
    $this->dpr = $dpr;
    return $this;
  }

  /**
   * Converts class field values to imgproxy processing options.
   *
   * @return string
   *   Imgproxy processing options.
   *
   * @see https://github.com/imgproxy/imgproxy/blob/master/docs/generating_the_url_advanced.md#processing-options
   */
  protected function getProcessingOptions(): string {
    $fields = [
      'resize' => 'rt',
      'width' => 'w',
      'height' => 'h',
      'dpr' => 'dpr',
      'extend' => 'ex',
      'enlarge' => 'el',
      'gravity' => 'g',
      'quality' => 'q',
      'background' => 'bg',
      'blur' => 'bl',
      'sharpen' => 'sh',
      'cachebuster' => 'cb',
      'extension' => 'f',
    ];
    $options = [];
    foreach ($fields as $field => $alias) {
      $value = $this->$field ?? NULL;
      if (!is_null($value)) {
        $strValue = is_bool($value)
          ? (string) (int) $value
          : (string) $value;
        $options[] = "$alias:$strValue";
      }
    }
    // Set classes.
    if ($this->watermark instanceof Watermark) {
      $strValue = implode(
        ':',
        [
          (string) $this->watermark->getOpacity(),
          (string) $this->watermark->getPosition(),
          (string) $this->watermark->getX(),
          (string) $this->watermark->getY(),
          (string) $this->watermark->getScale(),
        ]
      );
      $options[] = 'wm:' . $strValue;
    }
    if (!empty($this->presets)) {
      $options[] = 'pr:' . implode(':', $this->presets);
    }
    return implode('/', $options);
  }

  /**
   * Generates an imgproxy url based on set fields.
   *
   * @return string
   *   Url string to target image..
   */
  public function toString(): string {
    $encodedUrl = rtrim(strtr(base64_encode($this->imageUrl), '+/', '-_'), '=');
    $path = sprintf("/%s/%s", $this->getProcessingOptions(), $encodedUrl);
    if ($this->salt && $this->key) {
      $data = $this->salt . $path;
      $sha256 = hash_hmac('sha256', $data, $this->key, TRUE);
      $sha256Encoded = base64_encode($sha256);
      $signature = str_replace(["+", "/", "="], ["-", "_", ""], $sha256Encoded);
      $signedPath = "/" . $signature . $path;
    }
    else {
      $signedPath = "/insecure$path";

    }
    return $this->endpoint . $signedPath;
  }

  /**
   * Setter for the original image url.
   *
   * @param string $imageUrl
   *   Url string.
   *
   * @return self
   *   Self.
   */
  public function setImageUrl(string $imageUrl): self {
    $this->imageUrl = $imageUrl;
    return $this;
  }

  /**
   * Converts the class to string.
   *
   * @return string
   *   Url as string.
   */
  public function __toString() {
    return $this->toString();
  }

  /**
   * Setter for the quality field.
   *
   * @param int|null $quality
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setQuality(?int $quality): self {
    if (is_int($quality) && ($quality > 100 || $quality < 0)) {
      throw new InvalidArgumentException(
        'Quality value must be between 0 and 100 percents'
      );
    }
    $this->quality = $quality;
    return $this;
  }

  /**
   * Setter for the background field.
   *
   * @param string|null $background
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setBackground(?string $background): self {
    if (!is_null($background)) {
      if (!preg_match('/[0-9a-f]+/', $background) || strlen($background) > 8) {
        throw new \InvalidArgumentException(
          'Background color must be a hex color'
        );
      }
    }
    $this->background = $background;
    return $this;
  }

  /**
   * Setter for the blur field.
   *
   * @param float|null $blur
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setBlur(?float $blur): self {
    $this->blur = $blur;
    return $this;
  }

  /**
   * Setter for the sharpen field.
   *
   * @param float|null $sharpen
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setSharpen(?float $sharpen): self {
    $this->sharpen = $sharpen;
    return $this;
  }

  /**
   * Setter for the watermark field.
   *
   * @param \Drupal\imgproxy\Imgproxy\Watermark|null $watermark
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setWatermark(?Watermark $watermark): self {
    $this->watermark = $watermark;
    return $this;
  }

  /**
   * Setter for the cachebuster field.
   *
   * @param string|null $cachebuster
   *   New value.
   *
   * @return self
   *   Self.
   */
  public function setCachebuster(?string $cachebuster): self {
    $this->cachebuster = $cachebuster;
    return $this;
  }

  /**
   * Setter for the presets field.
   *
   * @param string[]|null $presets
   *   Preset names.
   *
   * @return self
   *   Self.
   */
  public function setPresets(?array $presets): self {
    if (is_array($presets)) {
      $presets = array_map(
        function ($item) {
          return (string) $item;
        },
        $presets
      );
    }
    $this->presets = $presets;
    return $this;
  }

}
