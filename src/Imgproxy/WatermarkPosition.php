<?php

namespace Drupal\imgproxy\Imgproxy;

use Drupal\imgproxy\ValidString;

/**
 * Watermark position.
 *
 * Specifies the position of the watermark in imgproxy.
 */
class WatermarkPosition extends ValidString {

  /**
   * Top edge.
   */
  const NORTH = 'no';

  /**
   * Bottom edge.
   */
  const SOUTH = 'so';

  /**
   * Right edge.
   */
  const EAST = 'ea';

  /**
   * Left edge.
   */
  const WEST = 'we';

  /**
   * Top-right corner.
   */
  const NORTHEAST = 'noea';

  /**
   * Top-left corner.
   */
  const NORTHWEST = 'nowe';

  /**
   * Bottom-Right corner.
   */
  const SOUTHEAST = 'soea';

  /**
   * Bottom-left corner.
   */
  const SOUTHWEST = 'sowe';

  /**
   * Center.
   */
  const CENTER = 'ce';

  /**
   * Replicate watermark to fill the whole image.
   */
  const REPLICATE = 're';

  /**
   * {@inheritdoc}
   */
  protected function getAllowedValues(): array {
    return [
      self::CENTER,
      self::NORTH,
      self::SOUTH,
      self::WEST,
      self::EAST,
      self::NORTHEAST,
      self::NORTHWEST,
      self::SOUTHEAST,
      self::SOUTHWEST,
      self::CENTER,
      self::REPLICATE,
    ];
  }

}
