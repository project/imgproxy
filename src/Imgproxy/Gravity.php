<?php

namespace Drupal\imgproxy\Imgproxy;

use Drupal\imgproxy\ValidString;

/**
 * Gravity is used when imgproxy decides to crop parts of the image.
 */
class Gravity extends ValidString {

  /**
   * Top edge.
   */
  const NORTH = 'no';

  /**
   * Bottom edge.
   */
  const SOUTH = 'so';

  /**
   * Right edge.
   */
  const EAST = 'ea';

  /**
   * Left edge.
   */
  const WEST = 'we';

  /**
   * Top-right corner.
   */
  const NORTHEAST = 'noea';

  /**
   * Top-left corner.
   */
  const NORTHWEST = 'nowe';

  /**
   * Bottom-Right corner.
   */
  const SOUTHEAST = 'soea';

  /**
   * Bottom-left corner.
   */
  const SOUTHWEST = 'sowe';

  /**
   * Center.
   */
  const CENTER = 'ce';

  /**
   * Smart. Let imgproxy decide where will be the center of the resulting image.
   */
  const SMART = 'sm';

  /**
   * {@inheritdoc}
   */
  protected function validateValue(string $value): bool {
    return in_array($value, $this->getAllowedValues())
      || preg_match('/^fp:(0(\.\d+)?|1(\.0+)?):(0(\.\d+)?|1(\.0+)?)$/', $value);
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllowedValues(): array {
    return [
      self::CENTER,
      self::NORTH,
      self::SOUTH,
      self::WEST,
      self::EAST,
      self::NORTHEAST,
      self::NORTHWEST,
      self::SOUTHEAST,
      self::SOUTHWEST,
      self::CENTER,
      self::SMART,
    ];
  }

  /**
   * Creates gravity from focus point.
   *
   * X and Y define the coordinates of the center of the resulting image.
   *
   * @param float $x
   *   X coordinate of the center. Can be from 0 (right) to 1 (left).
   * @param float $y
   *   Y coordinate of the center. Can be from 0 (top) to 1 (bottom).
   *
   * @return static
   */
  public static function fromFocusPoint(float $x, float $y): self {
    return new static(sprintf('fp:%01f:%01f', $x, $y));
  }

}
