<?php

namespace Drupal\imgproxy\Imgproxy;

/**
 * Watermark configuration for the image.
 *
 * @see https://github.com/imgproxy/imgproxy/blob/master/docs/watermark.md
 *
 * @todo add validation for values.
 */
class Watermark {

  /**
   * X offset.
   *
   * @var float
   */
  private $x;

  /**
   * Y offset.
   *
   * @var float
   */
  private $y;

  /**
   * Floating point number that defines watermark size relative to image size.
   *
   * @var float
   */
  private $scale;

  /**
   * Watermark position.
   *
   * @var \Drupal\imgproxy\Imgproxy\WatermarkPosition
   */
  private $position;

  /**
   * Watermark opacity.
   *
   * @var float
   */
  private $opacity;

  /**
   * Setter for the X field.
   *
   * @param float $x
   *   New value. Must be between 0 and 1.
   *
   * @return self
   *   This object.
   */
  public function setX(float $x): self {
    $this->x = $x;
    return $this;
  }

  /**
   * Setter for the Y field.
   *
   * @param float $y
   *   New value. Must be between 0 and 1.
   *
   * @return self
   *   This object.
   */
  public function setY(float $y): self {
    $this->y = $y;
    return $this;
  }

  /**
   * Setter for the scale field.
   *
   * @param float $scale
   *   New value. Set to 0 to disable.
   *
   * @return self
   *   This object.
   */
  public function setScale(float $scale): self {
    $this->scale = $scale;
    return $this;
  }

  /**
   * Setter for the position field.
   *
   * @param \Drupal\imgproxy\Imgproxy\WatermarkPosition $position
   *   New value.
   *
   * @return self
   *   This object.
   */
  public function setPosition(WatermarkPosition $position): self {
    $this->position = $position;
    return $this;
  }

  /**
   * Setter for the opacity field.
   *
   * @param float $opacity
   *   New value. Must be between 0 and 1.
   *
   * @return self
   *   This object.
   */
  public function setOpacity($opacity): self {
    $this->opacity = $opacity;
    return $this;
  }

  /**
   * Getter for scale field.
   */
  public function getScale() {
    return $this->scale;
  }

  /**
   * Getter for the opacity field.
   */
  public function getOpacity() {
    return $this->opacity;
  }

  /**
   * Getter for the position field.
   */
  public function getPosition() {
    return $this->position;
  }

  /**
   * Getter for the X.
   */
  public function getX() {
    return $this->x;
  }

  /**
   * Getter for the Y.
   */
  public function getY() {
    return $this->y;
  }

  /**
   * Class factory.
   *
   * @param float $opacity
   *   Opacity.
   * @param \Drupal\imgproxy\Imgproxy\WatermarkPosition|null $position
   *   Watermark position.
   * @param float $x
   *   X offset.
   * @param float $y
   *   Y offset.
   * @param float $scale
   *   Scale.
   *
   * @return static
   *   New instance.
   */
  public static function create($opacity, ?WatermarkPosition $position = NULL, $x = NULL, $y = NULL, $scale = NULL) {
    return (new static())
      ->setOpacity($opacity)
      ->setPosition($position)
      ->setX($x)
      ->setY($y)
      ->setScale($scale);
  }

}
