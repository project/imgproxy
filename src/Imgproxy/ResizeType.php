<?php

namespace Drupal\imgproxy\Imgproxy;

use Drupal\imgproxy\ValidString;

/**
 * Defines how imgproxy will resize the source image.
 */
class ResizeType extends ValidString {

  /**
   * Resize the image while keeping aspect ratio to fit given size.
   */
  const FIT = 'fit';

  /**
   * Crops the image to a given size.
   */
  const CROP = 'crop';

  /**
   * Resize the image while keeping aspect ratio to fill given size.
   */
  const FILL = 'fill';

  /**
   * {@inheritdoc}
   */
  protected function getAllowedValues(): array {
    return [
      self::FIT,
      self::CROP,
      self::FILL,
    ];
  }

}
