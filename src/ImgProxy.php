<?php

namespace Drupal\imgproxy;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\imgproxy\Imgproxy\Gravity;
use Drupal\imgproxy\Imgproxy\ImgProxyUrl;
use Drupal\imgproxy\Imgproxy\ResizeType;
use Drupal\imgproxy\Imgproxy\Watermark;
use Drupal\imgproxy\Imgproxy\WatermarkPosition;

/**
 * Main service for working with imgproxy.
 */
class ImgProxy {

  /**
   * Drupal config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ImgProxy constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory injection.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config->get('imgproxy.settings');
  }

  /**
   * Creates imgproxy url for given data.
   *
   * @param string|\Drupal\Core\Url $url
   *   Source image url. Must be accessible from imgproxy server.
   * @param array $settings
   *   Processing settings. The following keys are accepted:
   *   * (int) width - Required image width.
   *   * (int) height - Required image height.
   *   * (bool) enlarge - Enlarge the image to given width and height.
   *   * (bool) extend - Extend the image to given size.
   *   * (string) background - hexcolor (w/0 leading #) for image background.
   *   * (float) blur - size of the mask for blur.
   *   * (float) sharpen - size of the mask to use.
   *   * (string) resize - How to resize the image. Should be one of the
   *     \Drupal\imgproxy\Imgproxy\ResizeType constants.
   *   * (string) gravity - How the image should aligned on cropping. Should be
   *     one of the \Drupal\imgproxy\Imgproxy\Gravity constants.
   *   * (int) dpr - Number to how multiply the image for retina displays.
   *   * (int) quality - Percents amount for image quality (if can be applied)
   *   * (string) extension - Target image extension.
   *   * (string[]) presets - List of imgproxy presets to use.
   *   * (array) watermark - Watermark position of the image.
   *
   *   Watermark array accepts the following keys:
   *   * (float) opacity - Required. Watermark opacity.
   *   * (float) x - X offset of the watermark.
   *   * (float) y - Y offset of the watermark.
   *   * (float) scale - Relative watermark size to the resulting image.
   *   * (string) position - Watermark position. Can be one of
   *     \Drupal\imgproxy\Imgproxy\WatermarkPosition constants.
   *
   * @return string
   *   Processed image url for the given parameters.
   *
   * @see https://github.com/imgproxy/imgproxy/blob/master/docs/generating_the_url_advanced.md
   *   List of all the options accepted by imgproxy with some descriptions.
   * @see https://github.com/imgproxy/imgproxy/blob/master/docs/presets.md
   *   Presets in imgproxy.
   * @see https://github.com/imgproxy/imgproxy/blob/master/docs/watermark.md
   *   Watermarks in imgproxy.
   */
  public function getUrl($url, array $settings) {
    if ($url instanceof Url) {
      // Twig should not modify the original Url object.
      $url = $url->setAbsolute()->toString();
    }
    $url = $this->config->get('local')
      ? str_replace(['public:', 'private:'], 'local:/', $url)
      : file_create_url($url);
    $instance = (new ImgProxyUrl())
      ->setImageUrl($url)
      ->setEndpoint($this->config->get('endpoint'))
      ->setKey($this->config->get('key'))
      ->setSalt($this->config->get('salt'))
      ->setResize(
        isset($settings['resize']) ? new ResizeType($settings['resize']) : NULL
      )
      ->setGravity(
        isset($settings['gravity']) ? new Gravity($settings['gravity']) : NULL
      )
      ->setWidth($settings['width'] ?? NULL)
      ->setHeight($settings['height'] ?? NULL)
      ->setEnlarge($settings['enlarge'] ?? NULL)
      ->setExtend($settings['extend'] ?? NULL)
      ->setBackground($settings['background'] ?? NULL)
      ->setBlur($settings['blur'] ?? NULL)
      ->setSharpen($settings['sharpen'] ?? NULL)
      ->setDpr($settings['dpr'] ?? NULL)
      ->setExtension($settings['extension'] ?? NULL)
      ->setQuality($settings['quality'] ?? NULL)
      ->setPresets($settings['presets'] ?? NULL);
    if (isset($settings['watermark'], $settings['watermark']['opacity'])) {
      $instance->setWatermark(
        Watermark::create(
          $settings['watermark']['opacity'],
          isset($settings['watermark']['position'])
            ? new WatermarkPosition($settings['watermark']['position'])
            : NULL,
          $settings['watermark']['x'] ?? NULL,
          $settings['watermark']['y'] ?? NULL,
          $settings['watermark']['scale'] ?? NULL
        )
      );
      $instance->setCachebuster($settings['cachebuster'] ?? NULL);
    }
    return $instance->toString();
  }

}
