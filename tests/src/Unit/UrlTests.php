<?php

namespace Drupal\tests\imgproxy\Unit;

use Drupal\imgproxy\ImgProxy;
use Drupal\imgproxy\Imgproxy\ImgProxyUrl;
use Drupal\Tests\UnitTestCase;

/**
 * Class UrlTests.
 *
 * @group imgproxy
 */
class UrlTests extends UnitTestCase {

  /**
   * Encode the url as it required by imgproxy.
   *
   * @param string $url
   *   Url to encode.
   *
   * @return string
   *   Encoded url.
   */
  protected function encodeUrl(string $url): string {
    return rtrim(strtr(base64_encode($url), '+/', '-_'), '=');
  }

  /**
   * Returns random hex string.
   *
   * @return string
   *   Random hex string.
   */
  protected function getRandomHex(): string {
    $val = '';
    for ($i = 0; $i < 100; $i++) {
      $val .= chr(rand(65, 90));
    }
    return md5($val);
  }

  /**
   * Tests that endpoint and image url are on their places.
   */
  public function testUrlEndpointAndImageUrlAreCorrect() {
    $endpoint = $this->randomMachineName();
    $uri = $this->randomMachineName();
    $encodedUri = $this->encodeUrl($uri);

    $instance = new ImgProxyUrl();
    $instance->setEndpoint($endpoint);
    $instance->setImageUrl($uri);
    self::assertEquals(
      $endpoint . '/insecure//' . $encodedUri,
      (string) $instance
    );

    // Check on a single param.
    $width = random_int(0, 1000);
    $instance->setWidth($width);
    self::assertEquals(
      sprintf('%s/insecure/%s/%s', $endpoint, 'w:' . $width, $encodedUri),
      (string) $instance
    );

    $salt = $this->getRandomHex();
    $key = $this->getRandomHex();
    $instance->setKey($key);
    $instance->setSalt($salt);
    $result = $instance->toString();
    self::assertStringStartsWith($endpoint, $result);
    self::assertStringEndsWith(
      sprintf('/w:%s/%s', $width, $encodedUri),
      $result
    );
  }

  /**
   * Checks signing is correct.
   *
   * @dataProvider urlSignatureDataProvider
   */
  public function testUrlSignature($endpoint, $salt, $key, $imgUrl, $signature) {
    $url = (new ImgProxyUrl())
      ->setEndpoint($endpoint)
      ->setKey($key)
      ->setSalt($salt)
      ->setImageUrl($imgUrl);
    self::assertContains('/' . $signature . '/', $url->toString());
  }

  /**
   * Data provider for signature checks.
   *
   * @return array
   *   Data.
   */
  public function urlSignatureDataProvider() {
    return [
      [
        'mHELyCOR',
        '77da07f84b49241fcd63caadc4155307',
        'abe96b4ee6b351714960ae4ced3a0443',
        'kBQRP6th',
        'HzSMxOl-Txg3tPn1VX_d9IQOMp5F-UXSPC0sAQvlmCE',
      ],
      [
        'meOSMKek',
        '0440a3aa44294efd76832433a398ccbf',
        'bba1e8f9bd89b661e2ec1d59dea82739',
        'f4ULIdex',
        'TvSTIYfSUgy8li5qLYIuvkbU58iK-uhh-9-YhBYzCIg',
      ],
      [
        'aNZKL5rQ',
        'c4409879d1b40cd4e7c44493e35f1e48',
        '87e938d3032a2c709451ec15864a3ba3',
        'cGWUbaZX',
        '2-hKqnUhdVZ54qjC_JNa9W1FuIJzp8bdFXZqdcqG0S8',
      ],
      [
        'aj61tDyb',
        '4aa1f14487f96899707c16d83f7cd77b',
        '6590caaaabd23cba4796e2e5f8b0ee07',
        'htGiVE8V',
        'gpCtZw-dyBmmLG1N_lkJfOmozLpClvKmGIYfZ0gPJSc',
      ],
    ];
  }

  /**
   * Tests that arguments passed to result url, also checks service is working.
   */
  public function testImgProxyServicePassesAllArgs() {
    $endpoint = $this->randomMachineName();
    $url = $this->randomMachineName();
    $encodedUrl = $this->encodeUrl($url);
    $config = $this->getConfigFactoryStub([
      'imgproxy.settings' => [
        'endpoint' => $endpoint,
        'local' => FALSE,
      ],
    ]);
    $service = new ImgProxy($config);
    $result = $service->getUrl($url, [
      'width' => 100,
      'height' => 200,
      'enlarge' => TRUE,
      'extend' => TRUE,
      'background' => 'aabbcc',
      'blur' => 0.5,
      'sharpen' => 0.4,
      'resize' => 'crop',
      'gravity' => 'ce',
      'dpr' => 2,
      'extension' => 'jpg',
      'quality' => 25,
      'watermark' => [
        'opacity' => 0.5,
        'x' => 12,
        'y' => 13,
        'scale' => 0.1,
        'position' => 'nowe',
      ],
      'cachebuster' => 'hbz',
    ]);

    self::assertStringStartsWith($endpoint, $result);
    self::assertStringEndsWith($encodedUrl, $result);
    self::assertContains('/w:100/', $result);
    self::assertContains('/h:200/', $result);
    self::assertContains('/el:1/', $result);
    self::assertContains('/ex:1/', $result);
    self::assertContains('/bg:aabbcc/', $result);
    self::assertContains('/bl:0.5/', $result);
    self::assertContains('/sh:0.4/', $result);
    self::assertContains('/rt:crop/', $result);
    self::assertContains('/g:ce/', $result);
    self::assertContains('/dpr:2/', $result);
    self::assertContains('/f:jpg/', $result);
    self::assertContains('/q:25/', $result);
    self::assertContains('/wm:0.5:nowe:12:13:0.1/', $result);
    self::assertContains('/cb:hbz/', $result);

  }

}
